from elasticsearch import elasticsearch
from datetime import datetime
es = elasticsearch([{'host': '10.244.0.129', 'port': '30200'}])
es.index{index="my", doc_type="test", id=42, body={"any": "data", "timestamp": datetime.now()}}
es.get(index="my", doc_type="test", id=42)['_source']