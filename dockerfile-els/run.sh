#!/bin/sh
BASE=/elasticsearch
chown -R es:es /data
chmod 755 /data
# Set a random node name if not set.
if [ -z "${NODE_NAME}" ]; then
	NODE_NAME=`hostname`
fi
export NODE_NAME=${NODE_NAME}
export PATH=/elasticsearch/bin:$PATH

# es start
chpst -u es:es $BASE/bin/elasticsearch 
